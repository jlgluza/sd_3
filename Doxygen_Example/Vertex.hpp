/******************************************************************************
 *                                                                            *
 * file11                                                                     *
 *                                                                            *
 ******************************************************************************/

#ifndef VERTEX_HPP
#define VERTEX_HPP 1

#include <vector>
#include <string>

using namespace std;

/******************************************************************************
 *                                                                            *
 * VertexType                                                                 *
 *                                                                            *
 ******************************************************************************/

/// currently supported vertex types

enum VertexType
{
  Identity,
  VectorScalar,
  DerivativeVectorScalar,
  TripleVector,
  QuadrupleVector,
  FermionScalar,
  FermionVector,
  GhostVector,
  ScalarCT,
  FermionCT,
  VectorCT,
  VectorScalarCT
};

/******************************************************************************
 *                                                                            *
 * Vertex                                                                     *
 *                                                                            *
 ******************************************************************************/

struct Field;

/// QFT vertex

struct Vertex
{
  VertexType           _type;

  int                  _order;

  vector<string>       _value;

  vector<const Field*> _original_fields;

  vector<const Field*> _sorted_fields;

  /// n! for every n equal fields
  int                  _symmetry_factor;
};

typedef const Vertex* vertex_pointer;

#endif
