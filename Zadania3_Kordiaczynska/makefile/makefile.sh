#!/bin/bash

echo " " > makefile
echo -e "CC=gcc" >> makefile
echo -e "CFLAGS= -c" >> makefile
echo -e "SOURCES= " $1 $2 $3 >> makefile
echo -e "OBJCS= \$(SOURCES:.c=.o) \n\n" >> makefile
echo -e "output: main.exe \n\t./main.exe \n\n" >> makefile
echo -e "main.exe: \$(OBJCS) \n\t\$(CC) \$(OBJCS) -o main.exe \n\n" >> makefile
echo -e "%.o: %.c \n\t\$(CC) \$(CFLAGS) $< -o \$@ \n\n" >> makefile

make
rm *.o
./main.exe



