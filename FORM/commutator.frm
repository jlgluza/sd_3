*page 25
#-
Off statistics;
Symbols hbar,m;
Functions x,p,H;
Local [H,x] = H*x - x*H;
id H = p^2/(2*m);
Print;
.sort
l [H,x] =- 1/2*x*p*p*m^-1 + 1/2*p*p*x*m^-1;
repeat;
id x*p = p*x + hbar*i_;
endrepeat;
Print;
.end