* p. 30
Symbols last, secondlast, dummy;
Function F;
On statistics;
Local Fibonacci19 = F(1,1) * dummy^17;
repeat;
id F(last?, secondlast?) * dummy = F(last + secondlast, last);
endrepeat;
id F(last?, secondlast?) = last;
Print;
.end